﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace DemoWeb.Controllers
{
    public class DemoController : Controller
    {
        // GET: /Demo/Index
        [HttpGet]
        public ActionResult Index()
        {
            using (var dbContext = new Models.DemoWebEntities())
            {
                var products = dbContext.Products.ToList();
                return View("_Index", products);
            }
        }

        public JsonResult GetAllProduct()
        {
            using (var dbContext = new Models.DemoWebEntities())
            {
                var products = dbContext.Products.ToList();
                return Json(products, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult CreateProduct()
        {
            return View();
        }

        [HttpPost]
        public ActionResult CreateProduct(
            Models.CreateProductModel model)
        {
            if (this.ModelState.IsValid)
            {
                var product = new Models.Product
                {
                    Name = model.Name
                };
                using (var dbContext = new Models.DemoWebEntities())
                {
                    dbContext.Products.Add(product);
                    dbContext.SaveChanges();
                }
                return Redirect("Index");
            }
            return View(model);
        }

        public ActionResult Product(int productId)
        {
            return View(productId);
        }

        public JsonResult GetProduct(Models.Product product)
        {
            using (var dbContext = new Models.DemoWebEntities())
            {
                var products = dbContext
                    .Products
                    .FirstOrDefault(a => a.Id == product.Id);
                var id = 1;
                var receipt = dbContext
                    .Receipts.Find(id);
                var details = receipt.ReceiptDetails.ToList();
                var detail = details[0];
                var product1 = detail.Product;
                receipt.ReceiptDetails.Remove(detail);
                var product5 = new Models.Product();
                var product3 = product1.Products
                    .FirstOrDefault(a => a.Id == 3);
                product1.Products.Remove(product3);
                product1.Products.Remove(product5);

                var sql = new StringBuilder();
                sql.Append("SELECT TOP(1) Name FROM PRODUCT");

                var data = dbContext
                      .Database
                      .SqlQuery<Models.CreateProductModel>(sql.ToString());

                receipt.SpecialReceipt = new Models.SpecialReceipt();
                dbContext.SpecialReceipts.Remove(receipt.SpecialReceipt);
                return Json(products);
            }
        }
    }
}