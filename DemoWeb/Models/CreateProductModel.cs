﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DemoWeb.Models
{
    public class CreateProductModel
    {
        [Required(ErrorMessage ="Ten khong dc trung nha may che")]
        public string Name { get; set; }
    }
}